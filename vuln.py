from ctypes import *

libc = CDLL("libc.so.6")
POINTSIZE = sizeof(POINTER(c_char))

#CHOOSE VALUE 64 for heap corruption
def main():
	n = cInt()
	temp = cInt()

	n.val = get_num()
	print "Creating %d strings" % n.val
	
	temp.val = n.val * POINTSIZE #needed new var for overflow 
	print "Allocating %d bytes!" % temp.val

	if n.val > 0:
		pointy= cast(libc.malloc(temp.val), POINTER(POINTER(c_char))) 
			
		for i in range(0, n.val):
			new_str = get_str()
			
			pointy[i] = cast(libc.malloc(sizeof(c_char) * len(new_str)), POINTER(c_char))
			for j in range(0, len(new_str)):
				pointy[i][j] = new_str[j]	
						
	
def get_num():
	return int(raw_input("Enter a num!: "))	

def get_str():
	return raw_input("Enter a string!: ")

class cInt(Structure):
	_fields_ = [("val", c_int, 8)] 
	#making it only 8 bits for simplicity's sake


if __name__ == '__main__':
	main()

